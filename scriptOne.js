(() => {
//1------------
  const handleClick = async () => {
    try {
      let response = await fetch('https://randomuser.me/api/');
      let user = await response.json();
      let name = user.results[0].name;

      console.log(`${name.first} ${name.last}`);

      await fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      })

    } catch (err) {
      console.log(`упс... (${err})`);
    }
  }

  document.getElementById('get-post').onclick = handleClick;

//2----------
  const postUser = async (formElem) => {
    try {
      await fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: new FormData(formElem),
      })
    } catch (err) {
      console.log(`упс... (${err})`);
    }
  };

  document.getElementById('save').onclick = async (e) => {
    e.preventDefault();

    const first = document.getElementById("first").value;
    const last = document.getElementById("last").value;

    await postUser(document.getElementById("formElem"));
    alert(`${first} ${last}`);
  };

//3-----------------  
  const getUser = async () => {
    try {
      const response = await fetch('https://randomuser.me/api/');
      const data = await response.json();

      return data?.results[0];
    } catch (err) {
      console.log('упс… Что-то пошло не так');
      return null;
    }
  };

  document.getElementById('get').onclick = async function () {
    const user = await getUser();
    document.getElementById('first').value = user?.name?.first;
    document.getElementById('last').value = user?.name?.last;
  };


})();
